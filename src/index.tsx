import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { BrowserRouter, Route } from 'react-router-dom'
// @ts-ignore
import scriptjs from 'scriptjs'
import dotenv from 'dotenv'

import { configureStore } from 'store'
import { App } from 'containers'

import 'react-app-polyfill/ie11'
import 'react-app-polyfill/stable'
import 'flexibility/flexibility'

import '../node_modules/normalize.css/normalize.css'
import './index.css'

dotenv.config()

const { store, persistor } = configureStore()

class Root extends React.Component<{}, {loaded: boolean}> {
	public readonly state = {
		loaded: false
	}

	public render() {
		if (!this.state.loaded) return null

		return (
			<Provider store={store}>
				<PersistGate loading={null} persistor={persistor}>
					<BrowserRouter>
						<Route component={App} path='/:travelOne?/:travelTwo?/:travelThree?/:travelFour?/:travelFive?/:travelSix?'/>
					</BrowserRouter>
				</PersistGate>
			</Provider>
		)
	}
}

ReactDOM.render(<Root/>, document.getElementById('root'))
