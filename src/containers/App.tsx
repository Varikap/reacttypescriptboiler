import React, {Suspense} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { RouteComponentProps } from 'react-router-dom'

import { ReduxState } from 'store'

interface StateProps {
}
interface DispatchProps {
}
interface Props {}
type PropsUnion = StateProps & DispatchProps & Props & RouteComponentProps<Params>

interface State {}

interface Params {
	travelOne: string
	travelTwo: string
	travelThree: string
	travelFour: string
	travelFive: string
	travelSix: string
}

const encodingDivider = '--'

export class Component extends React.Component<PropsUnion, State> {
	public readonly state: State = {}

	constructor(props: PropsUnion) {
		super(props)

		const params = props.match.params
	}

	public componentDidUpdate(prevProps: Readonly<PropsUnion>, prevState: Readonly<State>, snapshot?: any): void {

	}

	public render() {
		return (
			<Suspense fallback={(<div>Loading</div>)}>
				<p>Content here</p>
			</Suspense>
		)
	}

}

const mapStateToProps = (state: ReduxState) => ({
})

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
}, dispatch)

export const App = connect<StateProps, DispatchProps, Props, ReduxState>(
	mapStateToProps,
	mapDispatchToProps
)(Component)
