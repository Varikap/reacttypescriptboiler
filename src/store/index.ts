// needs as; otherwise throws Import declaration conflicts with local declaration of 'ReduxState'
import { ReduxState as ReduxStatee } from './configureStore'

// NOTE: Normally barrel files can just re-export types, unfortunately CRA always enables the --isolatedModules flag,
// causing a TS1205 error. A workaround is to cast them to a type with the same name and export that.

export type ReduxState = ReduxStatee
export { configureStore } from './configureStore'
export { setZoomLevel
} from './application/actions'
