import { ActionType } from './actions'
import { Reducer } from 'redux'
import { Tooltip } from './lib/Tooltip'

export interface State {
	zoom: number
}

const initialState: State = {
	zoom: 10
}

export type ActionDispatch = SetZoomLevel

export const reducer: Reducer<State, ActionDispatch> = (state: State = initialState, action: ActionDispatch) => {
	switch (action.type) {
		case ActionType.SetZoomLevel:
			return reduceSetZoomLevel(state, action)
		default:
			return state
	}
}

interface SetZoomLevel {
	type: ActionType.SetZoomLevel
	data: number
}

const reduceSetZoomLevel = (state: State, action: SetZoomLevel) => {
	return {
		...state,
		zoom: action.data
	}
}
