import { ActionDispatch } from './reducer'
import { Tooltip } from './lib/Tooltip'

export enum ActionType {
	SetZoomLevel = '@@application/zoom'
}

export function setZoomLevel(level: number) {
	return (dispatch: (action: ActionDispatch) => {}) => {
		dispatch({
			type: ActionType.SetZoomLevel,
			data: level
		})
	}
}
