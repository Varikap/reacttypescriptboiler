export interface Tooltip {
	location: {
		lat: number
		lng: number
	}
	title: string
}
