// Example of exporting intefaces
// import { POIApiResponse } from './api/POIApiResponse'


// NOTE: Normally barrel files can just re-export types, unfortunately CRA always enables the --isolatedModules flag,
// causing a TS1205 error. A workaround is to cast them to a type with the same name and export that.

// export type POIApiResponse = POIApiResponse

// This export is needed for this example to prevent: 
// All files must be modules when the '--isolatedModules' flag is provided
export {}
